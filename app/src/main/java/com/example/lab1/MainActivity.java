package com.example.lab1;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
Button btn1,btn2;
    NotificationManager notifManager;
    Notification noti;
    NotificationCompat.Builder builder;
    String channelId = "default_channel_id";
    String channelDescription = "Default Channel";
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        anhxa();
        if (notifManager == null) {
            notifManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = notifManager.getNotificationChannel(channelId);
            if (notificationChannel == null) {
                int importance = NotificationManager.IMPORTANCE_HIGH; //Set the importance level
                notificationChannel = new NotificationChannel(channelId, channelDescription, importance);
                notificationChannel.setLightColor(Color.GREEN); //Set if it is necesssary
                notificationChannel.enableVibration(true); //Set if it is necesssary
                notifManager.createNotificationChannel(notificationChannel);
            }
            builder = new NotificationCompat.Builder(MainActivity.this, channelId);
            intent = new Intent(MainActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            builder.setContentTitle("nhac cua tui")                            // required
                    .setSmallIcon(R.drawable.ic_launcher_background)   // required
                    .setContentText("Do ta khong do nang") // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true);
        }else{
            builder = new NotificationCompat.Builder(MainActivity.this, channelId);
            intent = new Intent(MainActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            builder.setContentTitle("nhac cua tui")                            // required
                    .setSmallIcon(R.drawable.ic_launcher_background)   // required
                    .setContentText("Do ta khong do nang") // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setPriority(Notification.PRIORITY_HIGH);
        }
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //khỏi tạo đối tượng intent
                Intent intent=new Intent(MainActivity.this,labservices.class);

                //khởi tạo đôi tuong bundle
                Bundle b=new Bundle();
                //khai bao cac bo gia tri va gan vao bundle
                b.putInt("StuId",1);
                b.putString("StuName","John");
                b.putString("Class","LT14307");
                //gan bundle vao intent
                intent.putExtra("Student",b);
                //khoi tao service

                startService(intent);
                noti=builder.build();
                notifManager.notify(0, noti);


            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(MainActivity.this,labservices.class);
                stopService(intent);
            }
        });
    }

    private void anhxa() {
        btn1=findViewById(R.id.button1);
        btn2=findViewById(R.id.button2);


    }
}
