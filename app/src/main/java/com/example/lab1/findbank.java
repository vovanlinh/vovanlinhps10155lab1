package com.example.lab1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import java.util.ArrayList;
import java.util.List;

public class findbank extends AppCompatActivity {
Spinner spbank;
Button btnbank;
String text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_findbank);
        anhxa();
        List<String> list=new ArrayList<>();
        list.add("TP Bank");
        list.add("Dong A Bank");
        list.add("Viettin Bank");
        list.add("Techcom Bank");
        ArrayAdapter<String>adapter=new ArrayAdapter(this,android.R.layout.simple_spinner_item,list);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_single_choice);
        spbank.setAdapter(adapter);
        spbank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
text=spbank.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnbank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(findbank.this,FindbankService.class);
                intent.putExtra("Bank",text);
                startService(intent);
            }
        });
    }

    private void anhxa() {
        spbank=findViewById(R.id.spbank);
        btnbank=findViewById(R.id.btnfindbank);
    }
}
