package com.example.lab1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class gdactivity extends AppCompatActivity {
Button bai1,bai2,bai3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gdactivity);
        anhxa();
        bai1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(gdactivity.this,MainActivity.class);
                startActivity(intent);
            }
        });
        bai2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(gdactivity.this,bai1activity.class);
                startActivity(intent);
            }
        });
        bai3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(gdactivity.this,findbank.class);
                startActivity(intent);
            }
        });
    }

    private void anhxa() {
        bai1=findViewById(R.id.bai1);
        bai2=findViewById(R.id.bai2);
        bai3=findViewById(R.id.bai3);
    }
}
