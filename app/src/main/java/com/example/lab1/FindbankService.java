package com.example.lab1;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

public class FindbankService extends Service {
    public FindbankService() {
    }



    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String bank=intent.getStringExtra("Bank");
        if (bank.equals("TP Bank")){
            Toast.makeText(this,"Lai suat ngan hang "+bank+" la 2%",Toast.LENGTH_SHORT).show();
        }
        if (bank.equals("Dong A Bank")){
            Toast.makeText(this,"Lai suat ngan hang "+bank+" la 3%",Toast.LENGTH_SHORT).show();
        }
        if (bank.equals("Viettin Bank")){
            Toast.makeText(this,"Lai suat ngan hang "+bank+" la 4%",Toast.LENGTH_SHORT).show();
        }
        if (bank.equals("Techcom Bank")){
            Toast.makeText(this,"Lai suat ngan hang "+bank+" la 5%",Toast.LENGTH_SHORT).show();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
