package com.example.lab1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class bai1activity extends AppCompatActivity {
    public static final String FILER ="any_key";
    Button btntim;
EditText edtnhapten,edtnhapkt;
MyReceiver myReceiver;
int con;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bai1activity);
        anhxa();
        btntim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(edtnhapten.getText().toString().trim().isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please enter String",Toast.LENGTH_SHORT).show();
                }else if(edtnhapkt.getText().toString().trim().isEmpty()){
                    Toast.makeText(getApplicationContext(),"Please enter Character",Toast.LENGTH_SHORT).show();
                }else {
                    Intent intent=new Intent(bai1activity.this,findservice.class);
                    intent.putExtra("String",edtnhapten.getText().toString());
                    intent.putExtra("Character",edtnhapkt.getText().toString());
                    startService(intent);
                }
            }
        });
    }
private  void setReceiver(){
        myReceiver=new MyReceiver();
    IntentFilter intentFilter=new IntentFilter();
    intentFilter.addAction(FILER);
    LocalBroadcastManager.getInstance(this).registerReceiver(myReceiver,intentFilter);
};
    private void anhxa() {
        btntim=findViewById(R.id.btnb1);
        edtnhapten=findViewById(R.id.edt1);
        edtnhapkt=findViewById(R.id.edt2);
    }

    @Override
    protected void onStart() {
        setReceiver();
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private  class MyReceiver extends BroadcastReceiver{
        @Override
        public void onReceive(Context context, Intent intent) {
            con=intent.getIntExtra("Broad",0);
            Toast.makeText(getApplicationContext(),"So ky tu "+edtnhapkt.getText().toString()+ "vua nhap la: "+con,Toast.LENGTH_SHORT).show();
        }
    }
}
