package com.example.lab1;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

public class labservices extends Service {
    MediaPlayer player;
    @Override
    public void onCreate() {

        super.onCreate();
        Toast.makeText(this,"onCreat",Toast.LENGTH_LONG).show();
        player=MediaPlayer.create(this,R.raw.tungyeupda);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this,"onStartCommand",Toast.LENGTH_LONG).show();

        //lay bundle tu intent
        Bundle b = intent.getBundleExtra("Student");
        //laygia tri tu bundle
        int StuId = b.getInt("StuId");
        String StuName = b.getString("StuName");
        String Class = b.getString("Class");
        //hien thi ket qua len man hinh
        String content = "Them sinh vien thanh cong .\n Thong tin sinh vien:\n Sinh vien:" + StuId + " " + StuName+"\n lop: "+Class;
        //  content += "\nLop" + Class;
        Toast.makeText(this, content, Toast.LENGTH_LONG).show();

        player.start();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        Toast.makeText(this,"onDestroy",Toast.LENGTH_LONG).show();
        player.stop();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
